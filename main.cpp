#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = int;
using bVec = std::vector<bool>;
using bMatrix = std::vector<std::vector<bool>>;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using u = unsigned int;

int a_num(char letra) {
    return (letra - 'a');
}

int main() {
    std::cout << a_num('b') << "\n";
}
