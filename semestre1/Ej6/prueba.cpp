// prueba.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//
#include <algorithm>
#include <functional>
#include <iostream>
#include <unordered_map>
#include <vector>

using T = long int;
using S = long int;
struct usuario {
	T id;
	S tiempo;
};
using R = usuario;

template <typename Ftype>
R pop_min(std::vector<R>& lista, Ftype& cmp);

template <typename Ftype>
void push_new(std::vector<R>& lista, const R& new_element, Ftype& cmp);

template <typename Ftype>
R pop_min(std::vector<R>& lista, Ftype& cmp) {
	R x = lista.front();
	std::pop_heap(lista.begin(), lista.end(), cmp);
	lista.pop_back();
	return x;
}

template <typename Ftype>
void push_new(std::vector<R>& lista, const R &new_element, Ftype& cmp) {
	lista.push_back(new_element);
	std::push_heap(lista.begin(), lista.end(), cmp);
}

void leer_nuevo(R &nuevo_usuario) {
	std::cin >> nuevo_usuario.id >> nuevo_usuario.tiempo;
}

int main()
{
	long int num_cajas;
	long int num_clientes;
	long int nuevo_cliente;
	auto cmp = [] (const R &op1, const R &op2) {return op1.tiempo > op2.tiempo || op1.tiempo == op2.tiempo && op1.id > op2.id; };
	
	std::cin >> num_cajas >> num_clientes;
	while (num_cajas) {
		std::vector<R> lista;
		for (int i = 1; i <= num_cajas; ++i) push_new(lista, { i, 0 }, cmp);
		for (int i = 0; i < num_clientes; ++i) {
			std::cin >> nuevo_cliente;
			R extraido = pop_min(lista, cmp);
			extraido.tiempo += nuevo_cliente;
			push_new(lista, extraido, cmp);
		}
		std::cout << pop_min(lista, cmp).id << "\n";
		std::cin >> num_cajas >> num_clientes;
	}
}
