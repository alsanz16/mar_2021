// prueba.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <set>
#include <unordered_map>
#include <vector>
#include "ConjuntosDisjuntos.h"
using T = std::pair<int, int>;
void unir_adyacentes_total(ConjuntosDisjuntos& c, const T& pos1, int indice, const std::map<T, long int>& posiciones);
int get_maximo(ConjuntosDisjuntos& c, int N);

int main()
{
    //std::ifstream in("in.txt");
    //std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!

    char caracter_leido;
    int filas, columnas, contador;
    std::cin >> filas >> columnas;
    while (std::cin) {
        contador = 0;
        std::map<T, long int> posiciones;
        for (int i = 0; i < filas; ++i) {
            for (int j = 0; j < columnas; ++j) {
                std::cin.get(caracter_leido);
                while (caracter_leido != '#' && caracter_leido != ' ')  std::cin.get(caracter_leido);
                if (caracter_leido == '#')
                    posiciones[{i, j}] = contador++;
            }
        }
        ConjuntosDisjuntos c = ConjuntosDisjuntos(contador);
        for (auto it = posiciones.begin(); it != posiciones.end(); ++it)
            unir_adyacentes_total(c, it->first, it->second, posiciones);

        std::cout << c.maximo() << " ";

        long int n;
        int i, j;
        std::cin >> n;
        for (long int k = 0; k < n; ++k) {
            std::cin >> i >> j;
            i--, j--;
            if (!posiciones.count({ i,j })) {
                posiciones[{i, j}] = contador++;
                c.annadir_conjunto();
                unir_adyacentes_total(c, { i,j }, posiciones.at({ i,j }), posiciones);
            }
            std::cout << c.maximo() << " ";
        }
        std::cout << "\n";
        std::cin >> filas >> columnas;
    }
}



void unir_adyacentes_total(ConjuntosDisjuntos& c, const T& pos1, int indice, const std::map<T, long int>& posiciones) {
    T pos[] = {
        { pos1.first - 1, pos1.second - 1 }, // arr izq
        { pos1.first, pos1.second - 1 }, // arr
        { pos1.first + 1, pos1.second - 1 }, // arr der
        { pos1.first + 1 , pos1.second }, // der
        { pos1.first + 1 , pos1.second + 1}, // abajo der
        { pos1.first , pos1.second + 1 },  // abajo
        { pos1.first - 1, pos1.second + 1 }, // abajo izq
        { pos1.first - 1, pos1.second} }; // izq

    for (T& p : pos)
        if (posiciones.count(p))
            c.unir(indice, posiciones.at(p));
}