// prueba.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//
#include <climits>
#include <algorithm>
#include <functional>
#include <iostream>
#include <set>
#include <vector>

template <typename Ftype>
void push_new(std::vector<long int>& lista, long int new_element, const Ftype& cmp);

template <typename Ftype>
long int pop_min(std::vector<long int>& lista, const Ftype& cmp);

template <typename Ftype>
void push_new(std::vector<long int>& lista, long int new_element, const Ftype& cmp) {
	lista.push_back(new_element);
	std::push_heap(lista.begin(), lista.end(), cmp);
}

template <typename Ftype>
long int pop_min(std::vector<long int>& lista, const Ftype& cmp) {
	long int x = lista.front();
	std::pop_heap(lista.begin(), lista.end(), cmp);
	lista.pop_back();
	return x;
}

struct tMedianHeap {
	std::vector<long int> menores;
	std::vector<long int> mayores;
	long int mediana = LONG_MAX;
	const std::function<bool(long int, long int)> mayor_que = [](long int a1, long int a2) { return a1 < a2; };
	const std::function<bool(long int, long int)> menor_que = [](long int a1, long int a2) { return a1 > a2; };


	void insert(long int nuevo_elemento) {
		if (nuevo_elemento < mediana) push_new(menores, nuevo_elemento,  mayor_que);
		else push_new(mayores, nuevo_elemento, menor_que);
		rebalance();
		if (menores.size() >= mayores.size()) mediana = menores.front();
		else mediana = mayores.front();
	}

	void rebalance() {
		if (mayores.size() - menores.size() == 2) 
			push_new(menores, pop_min(mayores, menor_que), mayor_que);
		if (menores.size() - mayores.size() == 2)
			push_new(mayores, pop_min(menores, mayor_que), menor_que);
	}

	long int pop_median() {
		if (menores.empty() && mayores.empty()) throw std::domain_error("");
		if (menores.size() >= mayores.size()) return pop_min(menores, mayor_que);
		else return pop_min(mayores, menor_que);
	}

};

int main()
{	
	long int tam_entrada, instruccion;
	std::cin >> tam_entrada;
	while (std::cin) {
		tMedianHeap m1;
		for (int i = 0; i < tam_entrada; ++i) {
			std::cin >> instruccion;
			if (instruccion == 0)
				try {
				std::cout << m1.pop_median() << " ";
			}
			catch (std::domain_error) {
				std::cout << "ECSA ";
			}
			else m1.insert(instruccion);
		}
		std::cout << "\n";
		std::cin >> tam_entrada;

	}
}
