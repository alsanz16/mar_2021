// prueba.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <unordered_map>
#include <vector>
#include "Digrafo.h"
using T = std::pair<int,int>;

void leerGrafo(Digrafo &g, std::map<int,int> &grado_entrada) {
	long int numero_aristas, v, w;
	std::cin >> numero_aristas;
	for (long int i = 0; i < numero_aristas; ++i) {
		std::cin >> v >> w;
		g.ponArista(v, w);
		++grado_entrada[w];
	}
}

int encontrar_siguiente(int N, std::set<int>& visitados, std::map<int, int>& grado_entrada) {
	for (int i = 0; i < N; ++i) {
		if (!visitados.count(i) && !grado_entrada[i])
			return i;
	}
	return -1;
}

int main()
{
	
	//std::ifstream in("in.txt");
	//std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!

	int N;
	bool imposible;
	bool encontrado ;
	std::cin >> N;
	while (std::cin) {
		Digrafo g = Digrafo(N);
		std::map<int, int> grado_entrada;
		leerGrafo(g, grado_entrada);
		encontrado = false;
		for (auto& it : grado_entrada)
			if (it.second == g.V() - 1 && g.ady(it.first).size() == 0) {
				std::cout << "SI " << it.first << "\n";
				encontrado = true;
				break;
			}
		if (!encontrado) std::cout << "NO\n";
		std::cin >> N;
	}
}

