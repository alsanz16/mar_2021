// prueba.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//
#include <algorithm>
#include <functional>
#include <iostream>
#include <unordered_map>
#include <vector>

using T = std::string;
using S = long int;
struct usuario {
	T id;
	S urgencia;
	int llegada;
};
using R = usuario;

template <typename Ftype>
R pop_min(std::vector<R>& lista, Ftype& cmp);

template <typename Ftype>
void push_new(std::vector<R>& lista, R& new_element, Ftype& cmp);

template <typename Ftype>
R pop_min(std::vector<R>& lista, Ftype& cmp) {
	R x = lista.front();
	std::pop_heap(lista.begin(), lista.end(), cmp);
	lista.pop_back();
	return x;
}

template <typename Ftype>
void push_new(std::vector<R>& lista, const R &new_element, Ftype& cmp) {
	lista.push_back(new_element);
	std::push_heap(lista.begin(), lista.end(), cmp);
}

R leer_nuevo(long int contador) {
	R nuevo_usuario;
	std::cin >> nuevo_usuario.id >> nuevo_usuario.urgencia;
	nuevo_usuario.llegada = contador;
	return nuevo_usuario;
}

int main()
{
	long int num_eventos;
	long int numero_envios;
	long int contador = 0;
	char tipo_evento;
	R nuevo_usuario;
	std::cin >> num_eventos;
	auto cmp = [] (const R &op1, const R &op2) {return op1.urgencia < op2.urgencia || op1.urgencia == op2.urgencia && op1.llegada > op2.llegada; };
	while (num_eventos) {
		std::vector<R> lista;
		for (int i = 0; i < num_eventos; ++i) {
			std::cin >> tipo_evento;
			if (tipo_evento == 'I') push_new(lista, leer_nuevo(contador++), cmp);
			else if (tipo_evento == 'A') std::cout << pop_min(lista, cmp).id << "\n";
		}
		std::cout << "---\n";
		std::cin >> num_eventos;
	}
}
