// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
#include <algorithm>
#include <climits>
#include <iostream>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include "TreeSet_AVL.h"

int main() {
    int num_elementos, num_preguntas;
    int nuevo_elemento, nueva_pregunta;
    int respuesta;
    std::cin >> num_elementos;
    while (num_elementos != 0) {
        Set<int> conjunto;
        for (int i = 0; i < num_elementos; ++i) {
            std::cin >> nuevo_elemento;
            conjunto.insert(nuevo_elemento);
        }
        std::cin >> num_preguntas;
        for (int i = 0; i < num_preguntas; ++i) {
            std::cin >> nueva_pregunta;
            try {
                respuesta = conjunto.lower(nueva_pregunta);
                std::cout << respuesta << "\n";
            } catch(std::out_of_range) {
                std::cout << "??\n";
            }
        }
        std::cout << "---\n";
        std::cin >> num_elementos;
    }
}
