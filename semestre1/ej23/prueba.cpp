#include <algorithm>
#include <climits>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include "DigrafoValorado.h"

using T = long int;
using Grafo = DigrafoValorado<T>;
using Ar = AristaDirigida<T>;
using pair = std::pair<int, T>;
auto cmp = [](const pair& a1, const pair& a2) {return a1.second > a2.second; };
using tQ = std::priority_queue<pair, std::vector<pair>, decltype(cmp)>;
long int minimo_coste(const Grafo& g, int origen, int destino, int peso1);


long int leerGrafo(Grafo &g) {
	long int aristas, v, w, peso;
	
	std::vector<long int> cargas(g.V());
	for (int i = 0; i < g.V(); ++i) 
		std::cin >> cargas[i];

	std::cin >> aristas;
	for (long int i = 0; i < aristas; ++i) {
		std::cin >> v >> w >> peso;
		g.ponArista(Ar(v - 1, w - 1, peso + cargas[w-1]));
	}
	return cargas[0];
}


int main()
{

#ifndef DOMJUDGE
	std::ifstream in("in.txt");
	auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
	int N;
	std::cin >> N;
	while (N) {
		Grafo g = Grafo(N);
		long int coste1 = leerGrafo(g);
		std::vector<bool> visitados(N, false);

		long int coste_minimo = minimo_coste(g, 0, N - 1, coste1);
		if (coste_minimo == LONG_MAX) std::cout << "IMPOSIBLE\n";
		else std::cout << coste_minimo  << "\n";
		std::cin >> N;
	}

#ifndef DOMJUDGE
	std::cin.rdbuf(cinbuf);
	system("PAUSE");
#endif

}

long int minimo_coste(const Grafo& g, int origen, int destino, int peso1) {
	std::vector<long int> distancias(g.V(), LONG_MAX);
	std::vector<int> visitados(g.V(), false);
	std::vector<int> padre(g.V(), -1);
	tQ cola = tQ(cmp);
	distancias[origen] = peso1;
	
	cola.push({ origen, distancias[origen] });
	while (!cola.empty()) {
		pair u_ady = cola.top();
		int u = u_ady.first;
		if (u == destino) return distancias[u];
		cola.pop();
		visitados[u] = true;
		for (auto& v_ady : g.ady(u)) {
			int v = v_ady.hasta(); 
			long int coste_nuevo = distancias[u] + v_ady.valor();
			if (!visitados[v] && distancias[v] > coste_nuevo) {
				distancias[v] = coste_nuevo;
				padre[v] = u;
				cola.push({ v, distancias[v] });
			}
		}
	}
	return LONG_MAX;
}
