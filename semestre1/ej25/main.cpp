#include <cmath>
#include <climits>
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <list>
#include <set>
#include <queue>
#include <unordered_set>
#include <vector>
#include "GrafoValorado.h"
#include <algorithm>
#include <climits>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include "GrafoValorado.h"

using T = long int;
using Grafo = GrafoValorado<T>;
using Ar = Arista<T>;

using pair =  std::pair<int, long int>;

using pair_step = std::pair<int, int>;
using ret_pair = std::pair<int, long int>;
auto cmp = [](const pair& a1, const pair& a2) {return a1.second > a2.second; };
using tQ = std::priority_queue<pair, std::vector<pair>, decltype(cmp)>;
ret_pair minimo_coste(const Grafo& g, int origen, int destino);
void leerGrafo(Grafo &g);
int bfs(const Grafo &g, int origen, int destino);

void leerGrafo(Grafo &g) {
    long int aristas, v, w, peso;
    
    std::cin >> aristas;
    for (long int i = 0; i < aristas; ++i) {
        std::cin >> v >> w >> peso;
        g.ponArista(Ar(v - 1, w - 1, peso));
    }
}


int main()
{

#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
    int N;
    std::cin >> N;
    while (std::cin) {
        Grafo g = Grafo(N);
        leerGrafo(g);
        std::vector<bool> visitados(N, false);

        int k, origen, destino;
        std::cin >> k;
        for (int i = 0; i < k; ++i) {
            std::cin >> origen >> destino;
            int min_camino = bfs(g, origen-1, destino-1);
            if (min_camino != -1) {
                ret_pair djikstra = minimo_coste(g, origen-1, destino-1);
                std::cout << djikstra.second
                << (djikstra.first == min_camino ? " SI\n" : " NO\n");
            }
            else
                std::cout << "SIN CAMINO\n";
        }
        std::cout << "---\n";

        std::cin >> N;
    }

#ifndef DOMJUDGE
    std::cin.rdbuf(cinbuf);
    system("PAUSE");
#endif

}

int bfs(const Grafo &g, int origen, int destino) {
    std::queue<pair_step> siguientes;
    std::vector<bool> visitados(g.V(), false);
    siguientes.push({origen, 0});
    visitados[origen] = true;
    
    while (!siguientes.empty()) {
        pair_step u_vert = siguientes.front();
        siguientes.pop();
        
        int u = u_vert.first;
        if (u == destino) return u_vert.second;
        for (auto &v_ady : g.ady(u)) {
            int v = v_ady.otro(u);
            if (!visitados[v]) {
                visitados[v] = true;
                siguientes.push({v, u_vert.second+1});
            }
        }
    }
    return -1;
}

ret_pair minimo_coste(const Grafo& g, int origen, int destino) {
    std::vector<long int> distancias(g.V(), LONG_MAX);
    tQ cola = tQ(cmp);
    distancias[origen] = 0;
    
    std::vector<long int> pasos(g.V(), LONG_MAX);
    pasos[origen] = 0;

    cola.push({ origen, distancias[origen]});
    while (!cola.empty()) {
        pair u_ady = cola.top();
        cola.pop();

        int u = u_ady.first;

        for (auto& v_ady : g.ady(u)) {
            int v = v_ady.otro(u);
            long int coste_nuevo = distancias[u] + v_ady.valor();
            if (distancias[v] >= coste_nuevo) {
                if (distancias[v] > coste_nuevo) pasos[v] = pasos[u] + 1;
                else pasos[v] = std::min(pasos[u] + 1, pasos[v]);
                distancias[v] = coste_nuevo;
                cola.push({ v, distancias[v]});
            }
        }
    }
    return {pasos[destino], distancias[destino]};
}

