#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include "GrafoValorado.h"

template <class T>
void leerGrafo(GrafoValorado<T> &g) {
	long int aristas, v, w, peso;
	std::cin >> aristas;
	for (long int i = 0; i < aristas; ++i) {
		std::cin >> v >> w >> peso;
		g.ponArista(Arista<long int>(v-1, w-1, peso));
	}
}

bool bfs(const GrafoValorado<long int>& g, int origen, int objetivo, long int anchura);

int main()
{

#ifndef DOMJUDGE
	std::ifstream in("in.txt");
	auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
	int N, K;
	int origen, destino;
	long int anchura;
	std::cin >> N;
	while (std::cin) {
		GrafoValorado<long int> g = GrafoValorado<long int>(N);
		leerGrafo(g);
		std::cin >> K;
		for (int i = 0; i < K; ++i) {
			std::cin >> origen >> destino >> anchura;
			std::cout << (bfs(g, origen - 1, destino -1 , anchura) ? "SI\n" : "NO\n");
		}
		std::cin >> N;
	}

#ifndef DOMJUDGE
	std::cin.rdbuf(cinbuf);
	system("PAUSE");
#endif

}

bool bfs(const GrafoValorado<long int> &g, int origen, int objetivo, long int anchura) {
	if (origen == objetivo) return true;
	std::unordered_set<int> visitados;
	std::queue<int> siguientes;
	siguientes.push(origen);
	visitados.insert(origen);

	while (!siguientes.empty()) {
		int u = siguientes.front();
		siguientes.pop();
		for (auto &ady : g.ady(u)) {
			int v = ady.otro(u);
			if (ady.valor() >= anchura) {
				if (v == objetivo) return true;
				else if (!visitados.count(v)) {
					siguientes.push(v);
					visitados.insert(v);
				}
			}
		}
	}
	return false;
}