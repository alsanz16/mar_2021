// prueba.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//
#include <cmath>
#include <climits>
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <set>
#include <queue>
#include <vector>


template <typename Ftype, typename Vtype>
void push_new(std::vector<Vtype>& lista, Vtype new_element, const Ftype& cmp);

template <typename Ftype, typename Vtype>
Vtype pop_min(std::vector<long int>& lista, const Ftype& cmp);

template <typename Ftype, typename Vtype>
void push_new(std::vector<Vtype>& lista, long int new_element, const Ftype& cmp) {
	lista.push_back(new_element);
	std::push_heap(lista.begin(), lista.end(), cmp);
}

template <typename Ftype, typename Vtype>
Vtype pop_min(std::vector<Vtype>& lista, const Ftype& cmp) {
	Vtype x = lista.front();
	std::pop_heap(lista.begin(), lista.end(), cmp);
	lista.pop_back();
	return x;
}

struct musico {
	long int num_musicos;
	long int atriles;
	long int proporcion;
};

int main()
{	
	long int n, p;
	long int nuevo_musico;
	auto cmp = [](auto m1, auto m2) {return m1.proporcion < m2.proporcion; };
	std::cin >> p >> n;
	while (std::cin) {
		p -= n;
		std::priority_queue<musico, std::vector<musico>, decltype(cmp)> reparto(cmp);
		for (int i = 0; i < n; ++i) {
			std::cin >> nuevo_musico;
			reparto.push({ nuevo_musico, 1, nuevo_musico });
		}
		while (p) {
			auto max_musico = reparto.top();
			reparto.pop();
			long double proporcion = ((long double) max_musico.num_musicos)/++max_musico.atriles;
			reparto.push({ max_musico.num_musicos, max_musico.atriles, (long int) std::ceil(proporcion) });
			--p;
		}
		std::cout << reparto.top().proporcion << "\n";
		std::cin >> p >> n;
	}
}
