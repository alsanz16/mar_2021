#include <algorithm>
#include <climits>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include "GrafoValorado.h"

using T = long int;
using Grafo = GrafoValorado<T>;
using Ar = Arista<T>;
using pair = std::pair<int, T>;
auto cmp = [](const pair& a1, const pair& a2) {return a1.second > a2.second; };
using tQ = std::priority_queue<pair, std::vector<pair>, decltype(cmp)>;
using tCaminos = std::vector<std::unordered_set<int>>;
long int minimo_coste(const Grafo& g, int origen, int destino);
void leerGrafo(Grafo &g) {
	long int aristas, v, w, peso;
	
	std::cin >> aristas;
	for (long int i = 0; i < aristas; ++i) {
		std::cin >> v >> w >> peso;
		g.ponArista(Ar(v - 1, w - 1, peso));
	}
}


int main()
{

#ifndef DOMJUDGE
	std::ifstream in("in.txt");
	auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
	int N;
	std::cin >> N;
	while (std::cin) {
		Grafo g = Grafo(N);
		leerGrafo(g);

		std::cout << minimo_coste(g, 0, N - 1) << std::endl;
		std::cin >> N;
	}

#ifndef DOMJUDGE
	std::cin.rdbuf(cinbuf);
	system("PAUSE");
#endif

}

long int minimo_coste(const Grafo& g, int origen, int destino) {
	std::vector<long int> distancias(g.V(), LONG_MAX);
	std::vector<long int> cuenta(g.V(), 0);
	std::vector<bool> visitados(g.V());
	tQ cola = tQ(cmp);
	distancias[origen] = 0;
	cuenta[origen] = 1;

	visitados[origen] = true;
	cola.push({ origen, distancias[origen] });
	while (!cola.empty()) {
		pair u_ady = cola.top();
		cola.pop();
		int u = u_ady.first;

		for (auto& v_ady : g.ady(u)) {
			int v = v_ady.otro(u);
			long int coste_nuevo = distancias[u] + v_ady.valor();
			if (distancias[v] >= coste_nuevo) {
				cuenta[v] = distancias[v] == coste_nuevo ? cuenta[v] + cuenta[u] : cuenta[u];
				distancias[v] = coste_nuevo;
				if (!visitados[v]) {
					cola.push({ v, distancias[v] });
					visitados[v] = true;
				}
			}
		}
	}
	return cuenta[destino];
}