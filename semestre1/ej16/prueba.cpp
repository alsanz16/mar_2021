// prueba.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "Digrafo.h"
using T = std::pair<int, int>;
const int N = 10000;
int main()
{
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

    Digrafo g = Digrafo(N);
    for (int i = 0; i < N; ++i) {
        g.ponArista(i, (i + 1) % N);
        g.ponArista(i, (i * 2) % N);
        g.ponArista(i, (i / 3) % N);
    }

    std::vector<bool> visitados(N);
    bool alcanzado = true;
    int inicio, fin, pasos;
    std::cin >> inicio >> fin;
    while (std::cin) {
        alcanzado = (inicio == fin);
        pasos = 0;
        std::queue<T> siguientes;
        siguientes.push({ inicio, 0 });
        std::fill(visitados.begin(), visitados.end(), false);

        while (!alcanzado) {
            T actual = siguientes.front();
            siguientes.pop();
            visitados[actual.first] = true;
            auto &adyacentes = g.ady(actual.first);

            for (auto it = adyacentes.begin(); it != adyacentes.end() && !alcanzado; ++it) {
                if (*it == fin) {
                    pasos = actual.second + 1;
                    alcanzado = true;
                }
                else if (!visitados[*it]) {
                    visitados[*it] = true;
                    siguientes.push({ *it, actual.second + 1 });
                }
            }
        }
        std::cout << pasos << "\n";
        std::cin >> inicio >> fin;
    }


    // para dejar todo como estaba al principio
#ifndef DOMJUDGE
    std::cin.rdbuf(cinbuf);
    system("PAUSE");
#endif
    
}

