#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include "Digrafo.h"
using T = std::pair<int, int>;
int bfs(int n, int k, const std::unordered_map<int, int>& conexiones);

void leerGrafo(std::unordered_map<int, int>& conexiones) {
	long int num_serpientes, num_escaleras, v, w;
	std::cin >> num_serpientes >> num_escaleras;
	for (long int i = 0; i < num_serpientes + num_escaleras; ++i) {
		std::cin >> v >> w;
		conexiones[v] = w;
	}
}

int main()
{

#ifndef DOMJUDGE
	std::ifstream in("in.txt");
	auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
	int N, K;
	std::cin >> N >> K;
	while (N) {
		std::unordered_map<int, int> conexiones;
		leerGrafo(conexiones);
		std::cout << bfs(N, K, conexiones) << std::endl;
		std::cin >> N >> K;
	}

#ifndef DOMJUDGE
	std::cin.rdbuf(cinbuf);
	system("PAUSE");
#endif

}

int bfs(int n, int k, const std::unordered_map<int, int>& conexiones) {
	if (n == 1) return 0;
	std::unordered_set<int> visitados;
	std::queue<T> siguientes;
	int objetivo = n * n;
	siguientes.push({ 1, 0 });
	visitados.insert(1);

	while (!siguientes.empty()) {
		T u_paso = siguientes.front();
		int u = u_paso.first;
		siguientes.pop();
		for (int j = 1; j <= k && u + j <= objetivo; ++j) {
			int v = (conexiones.count(u + j) ? conexiones.at(u + j) : u + j);
			if (!visitados.count(v))
				if (v == objetivo) return u_paso.second + 1;
				else {
					siguientes.push({ v, u_paso.second + 1 });
					visitados.insert(v);
				}
		}
	}
}