// prueba.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//
#include <algorithm>
#include <functional>
#include <iostream>
#include <unordered_map>
#include <vector>

using T = long int;
using S = long int;
struct usuario {
	T id;
	S periodo;
};
using R = usuario;

template <typename Ftype>
R pop_min(std::vector<R>& lista, Ftype& cmp);

template <typename Ftype>
void push_new(std::vector<R>& lista, R& new_element, Ftype& cmp);

void print_envios(std::vector<R>& lista, int k) {
	R extraido;
	auto cmp = [](R op1, R op2) { return op1.periodo > op2.periodo || op1.periodo == op2.periodo && op1.id > op2.id; };
	std::unordered_map<T, S> periodos;
	std::make_heap(lista.begin(), lista.end(), cmp);
	for (int i = 0; i < k; ++i) {
		extraido = pop_min(lista, cmp);
		if (!periodos.count(extraido.id))
			periodos[extraido.id] = extraido.periodo;
		std::cout << extraido.id << "\n";
		extraido.periodo += periodos[extraido.id];
		push_new(lista, extraido, cmp);
	}
	std::cout << "---\n";
}

template <typename Ftype>
R pop_min(std::vector<R>& lista, Ftype& cmp) {
	R x = lista.front();
	std::pop_heap(lista.begin(), lista.end(), cmp);
	lista.pop_back();
	return x;
}

template <typename Ftype>
void push_new(std::vector<R>& lista, R &new_element, Ftype& cmp) {
	lista.push_back(new_element);
	std::push_heap(lista.begin(), lista.end(), cmp);
}


int main()
{
	long int num_usuarios;
	long int numero_envios;
	R nuevo_usuario;
	std::cin >> num_usuarios;
	while (num_usuarios) {
		auto lista = std::vector<R>(num_usuarios);
		for (int i = 0; i < num_usuarios; ++i) 
			std::cin >> lista[i].id >> lista[i].periodo;
		std::cin >> numero_envios;
		print_envios(lista, numero_envios);

		std::cin >> num_usuarios;
	}
}
