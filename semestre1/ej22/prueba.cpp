#include <algorithm>
#include <climits>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include "ConjuntosDisjuntos.h"
#include "GrafoValorado.h"

using T = long int;
using Ar = Arista<T>;
auto cmp = [](const Ar& a1, const Ar& a2) {return a1.valor() > a2.valor(); };
using tLista = std::priority_queue<Ar, std::vector<Ar>, decltype(cmp)>;
long int minimo_coste(ConjuntosDisjuntos& islas, tLista& puentes);


void leerGrafo(tLista &cola_aristas) {
	long int aristas, v, w, peso;
	std::cin >> aristas;
	for (long int i = 0; i < aristas; ++i) {
		std::cin >> v >> w >> peso;
		cola_aristas.push(Ar(v - 1, w - 1, peso));
	}
}


int main()
{

#ifndef DOMJUDGE
	std::ifstream in("in.txt");
	auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
	int N;
	std::cin >> N;
	while (std::cin) {
		ConjuntosDisjuntos islas(N);
		tLista puentes = tLista(cmp);
		leerGrafo(puentes); 
		long int coste = minimo_coste(islas, puentes);
		if (coste == -1) std::cout << "No hay puentes suficientes\n";
		else std::cout <<  coste << "\n";
		std::cin >> N;
	}

#ifndef DOMJUDGE
	std::cin.rdbuf(cinbuf);
	system("PAUSE");
#endif

}

long int minimo_coste(ConjuntosDisjuntos &islas, tLista &puentes) {
	long int coste = 0;
	while (!puentes.empty() && islas.num_cjtos() > 1) {
		const Ar top = puentes.top();
		puentes.pop();
		if (islas.unir(top.uno(), top.otro(top.uno()))) coste += top.valor();
	}
	if (islas.num_cjtos() == 1) return coste;
	else return -1;
}