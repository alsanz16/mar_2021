//
//  ConjuntosDisjuntos.h
//
//  Implementación de estructuras de partición o conjuntos disjuntos
//  con unión por tamaño y compresión de caminos
//
//  Facultad de Informática
//  Universidad Complutense de Madrid
//
//  Copyright (c) 2020  Alberto Verdejo
//

#ifndef CONJUNTOSDISJUNTOS_H_
#define CONJUNTOSDISJUNTOS_H_

#include <vector>
#include <iostream>

class ConjuntosDisjuntos {
protected:
   long int ncjtos;  // número de conjuntos disjuntos
   long int max_size;
   mutable std::vector<long int> p;  // enlace al padre
   std::vector<long int> tam;  // tamaño de los árboles
public:
   
   // crea una estructura de partición con los elementos 0..N-1,
   // cada uno en un conjunto, partición unitaria
   ConjuntosDisjuntos(long int N) : ncjtos(N), p(N), tam(N,1), max_size(N > 0) {
      for (long int i = 0; i < N; ++i)
        p[i] = i;   
   }
   
   //  devuelve el representante del conjunto que contiene a a
   long int buscar(int a) const {
      if (p.at(a) == a) // es una raíz
         return a;
      else
         return p[a] = buscar(p[a]);
   }
   
   // unir los conjuntos que contengan a a y b
   bool unir(long int a, long int b) {
      long int i = buscar(a);
      long int j = buscar(b);
      if (i == j) return false;
      if (tam[i] > tam[j]) { // i es la raíz del árbol más grande
         tam[i] += tam[j]; p[j] = i;
         max_size = std::max(max_size, tam[i]);
         tam[j] = -1;
      } else {
         tam[j] += tam[i]; p[i] = j;
         max_size = std::max(max_size, tam[j]);
         tam[i] = -1;
      }
      --ncjtos;
      return true;
   }

   // devuelve si a y b están en el mismo conjunto
   bool unidos(long int a, long int b) const {
      return buscar(a) == buscar(b);
   }
   
   // devuelve el número de elementos en el conjunto de a
   long int cardinal(long int a) const {
      return tam[buscar(a)];
   }

   long int maximo() const {
       return max_size;
   }
   
   // devuelve el número de conjuntos disjuntos
   long int num_cjtos() const { return ncjtos; }

   void annadir_conjunto()  {
       p.push_back(p.size());
       long int u = 1;
       tam.push_back(u);
       max_size = std::max( max_size, u );

   }
};

#endif
