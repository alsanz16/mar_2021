#include <cmath>
#include <climits>
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <list>
#include <set>
#include <queue>
#include <unordered_map>
#include <vector>
#include "Grafo.h"

bool es_ciclo(const Grafo &g, std::set<int> &visitados, const int origen);
int main()
{
//std::ifstream in("in.txt");
//std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!

try {
    
 Grafo g(std::cin);
 while (true) {
  std::set<int> visitados;
  std::cout << (!es_ciclo(g, visitados, 0) && visitados.size() == g.V() ? "SI\n" : "NO\n");
  g = Grafo(std::cin);
  }
} catch (std::domain_error) { return 0; }
}

bool es_ciclo(const Grafo &g, std::set<int> &visitados, const int origen) {
    std::queue<int> siguientes;
    std::vector<int> padres(g.V(), -1);
    visitados.insert(origen);
    siguientes.push(origen);

    while (!siguientes.empty()) {

      int u = siguientes.front();
      siguientes.pop();
      for (auto v : g.ady(u)) {
          if (!visitados.count(v)) {
              visitados.insert(v);
              siguientes.push(v);
              padres[v] = u;
          }
          else if (padres[u] != v)
              return true;
      }
  }
  return false;
}
