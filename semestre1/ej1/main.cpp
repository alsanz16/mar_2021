// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
#include <algorithm>
#include <climits>
#include <iostream>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include "bintree_eda.h"

struct tResult {
    bool busqueda;
    bool equilibrado;
    int altura;
    int max;
    int min;
};
tResult esAVL(const bintree<int> &arbol);

tResult esAVL(const bintree<int> &arbol) {
    if (arbol.empty()) return {true, true, 0, INT_MIN, INT_MAX};
    tResult left = esAVL(arbol.left());
    tResult right = esAVL(arbol.right());
    return {left.busqueda && right.busqueda && right.min > arbol.root() && arbol.root() > left.max,
        left.equilibrado && right.equilibrado && abs(left.altura-right.altura) <= 1,
        std::max(left.altura, right.altura) + 1,
        std::max(right.max, arbol.root()),
        std::min(left.min, arbol.root())
    };
}


int main() {
    int cuenta;
    std::cin >> cuenta;
    for (int i = 0; i < cuenta; ++i) {
        tResult es_avl = esAVL(leerArbol(-1));
        std::cout << (es_avl.equilibrado && es_avl.busqueda ? "SI\n" : "NO\n");
    }
}
