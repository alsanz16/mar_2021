// prueba.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//
#include <algorithm>
#include <functional>
#include <iostream>
#include <set>
#include <vector>

struct tPeriodo {
	long int comienzo;
	long int fin;
};

int main()
{	
	long int num_simples, num_periodicos, tiempo;
	long int inicio, fin, periodo;
	bool error;
	tPeriodo nuevo_periodo;
	auto cmp = [&error](const tPeriodo &op1, const tPeriodo &op2) {
		if (op1.fin <= op2.comienzo) return true;
		else if (op2.fin <= op1.comienzo) return false;
		else throw std::domain_error("");
	};
	
	std::set<tPeriodo, decltype(cmp)> s(cmp);
	std::cin >> num_simples >> num_periodicos >> tiempo;
	while (std::cin) {
		error = false;
		for (int i = 0; i < num_simples; ++i) {
			std::cin >> inicio >> fin;
			if (inicio < tiempo && !error) try {
				s.insert({ inicio, std::min(fin, tiempo) });
			}
			catch (std::domain_error &e) {
				error = true; 
			}
		}
		
		for (int i = 0; i < num_periodicos; ++i) {
			std::cin >> inicio >> fin >> periodo;
			for (int j = 0; inicio + j < tiempo && !error; j += periodo) {
				try {
					s.insert({ inicio + j, std::min(fin + j, tiempo) });
				}
				catch (std::domain_error& e) {
					error = true;
				}
			}
		}

		std::cout << (error ? "SI" : "NO") << "\n";	
		s.clear();
		std::cin >> num_simples >> num_periodicos >> tiempo;

	}
}
