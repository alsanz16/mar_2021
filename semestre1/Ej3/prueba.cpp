// prueba.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//
#include <algorithm>
#include <iostream>
#include <vector>
long int pop_min(std::vector<long int>& lista);

long int min_suma(std::vector<long int>& lista) {
	long int op1, op2, suma = 0;
	auto cmp = [](long int a, long int b) {return a > b; };
	std::make_heap(lista.begin(), lista.end(), cmp);
	while (lista.size() > 1) {
		op1 = pop_min(lista);
		op2 = pop_min(lista);
		suma += op1 + op2;
		lista.push_back(op1 + op2);
		std::push_heap(lista.begin(), lista.end(), cmp);
	}
	return suma;
}

long int pop_min(std::vector<long int>& lista) {
	auto cmp = [](long int a, long int b) {return a > b; };
	long int x = lista.front();
	std::pop_heap(lista.begin(), lista.end(), cmp);
	lista.pop_back();
	return x;
}

int main()
{
	long int num_sumandos, nuevo_sumando;
	std::cin >> num_sumandos;
	while (num_sumandos) {
		auto lista = std::vector<long int>(num_sumandos);
		for (long int i = 0; i < num_sumandos; ++i) {
			std::cin >> nuevo_sumando;
			lista[i] = nuevo_sumando;
		}
		std::cout << min_suma(lista) << "\n";
		std::cin >> num_sumandos;

	}
}
