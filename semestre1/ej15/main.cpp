#include <cmath>
#include <climits>
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <list>
#include <set>
#include <queue>
#include <unordered_set>
#include <vector>
#include "Grafo.h"
using T = std::pair<int, int>;
void bfs(const Grafo &g, std::unordered_set<int> &visitados, const int origen, const int ttl);
int get_next_unvisited_node(const Grafo &g, const std::unordered_set<int> &visitados);

void read_new_graph(Grafo &g) {
    long int aristas;
    int  v, w;
    std::cin >> aristas;
    for (long int i = 0; i < aristas; ++i) {
        std::cin >> v >> w;
        g.ponArista(v-1, w-1);
    }
}

int main()
{
    //std::ifstream in("in.txt");
    //std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!
    
    int nodos, num_consultas, nodo_inicial, ttl;
    std::unordered_set<int> visitados;
    std::cin >> nodos;
    while (std::cin){
        Grafo g = Grafo(nodos);
        read_new_graph(g);
        std::cin >> num_consultas;
        for (int i = 0; i < num_consultas; ++i) {
            std::cin >> nodo_inicial >> ttl;
            bfs(g, visitados, nodo_inicial-1, ttl);
            std::cout << g.V() - visitados.size() << "\n";
            visitados.clear();
        }
        std::cout << "---\n";
        std::cin >> nodos;


    }
}

int get_next_unvisited_node(const Grafo &g, const std::unordered_set<int> &visitados) {
    for (int i = 0;i < g.V(); ++i)
        if (!visitados.count(i)) return i;
    return -1;
}

void bfs(const Grafo &g, std::unordered_set<int> &visitados, const int origen, const int ttl) {
    std::queue<T> siguientes;
    visitados.insert(origen);
    siguientes.push({origen, ttl});

    while (!siguientes.empty()) {
        T u_ttl = siguientes.front();
        siguientes.pop();

        int u = u_ttl.first;
        int actual_ttl = u_ttl.second;
        if (!actual_ttl) continue;
        for (auto v : g.ady(u)) {
          if (!visitados.count(v)) {
              visitados.insert(v);
              siguientes.push({v, actual_ttl-1});
          }
        }
  }
}
