#include <cmath>
#include <climits>
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <list>
#include <unordered_map>
#include <set>
#include <queue>
#include <unordered_set>
#include <vector>
#include "Grafo.h"
using T = std::pair<int, bool>;
int get_next_unvisited_node(const Grafo &g, const std::unordered_set<int> &visitados);
int bfs(const Grafo &g, std::unordered_set<int> &visitados, const int origen);

void read_new_graph(Grafo &g) {
    long int aristas;
    int  v, w;
    std::cin >> aristas;
    for (long int i = 0; i < aristas; ++i) {
        std::cin >> v >> w;
        g.ponArista(v-1, w-1);
    }
}

int main()
{
    //std::ifstream in("in.txt");
    //std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!

    std::unordered_set<int> visitados;
    int nodos;
    std::cin >> nodos;
    while (std::cin ){
        Grafo g = Grafo(nodos);
        read_new_graph(g);
        int suma = 0;
        int nuevo_componente = 0;
        while (visitados.size() < g.V() && nuevo_componente != -1) {
            nuevo_componente =  bfs(g, visitados, get_next_unvisited_node(g, visitados));
            suma += nuevo_componente;
        }
        visitados.clear();
        if (nuevo_componente == -1) std::cout << "IMPOSIBLE\n";
        else std::cout << suma << "\n";
        std::cin >> nodos;

    }
}

int get_next_unvisited_node(const Grafo &g, const std::unordered_set<int> &visitados) {
    for (int i = 0;i < g.V(); ++i)
        if (!visitados.count(i)) return i;
    return -1;
}

int bfs(const Grafo &g, std::unordered_set<int> &visitados, const int origen) {
    std::unordered_set<int> colores;
    std::queue<int> siguientes;
    int ya_visitados = visitados.size();
    visitados.insert(origen);
    siguientes.push(origen);
    
    while (!siguientes.empty()) {
        int u = siguientes.front();
        siguientes.pop();
        for (auto v : g.ady(u)) {
          if (!visitados.count(v)) {
              visitados.insert(v);
              if(!colores.count(u)) colores.insert(v);
              siguientes.push(v);
          }
          else if (colores.count(v) && colores.count(u) or !colores.count(v) && !colores.count(u))
              return -1;
        }
    }
    return std::min(visitados.size()-ya_visitados-colores.size(), colores.size());
}
