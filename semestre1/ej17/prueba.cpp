// prueba.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <unordered_map>
#include <vector>
#include "Digrafo.h"
using T = std::pair<int,int>;

void leerGrafo(Digrafo &g, std::map<int,int> &grado_entrada) {
	long int numero_aristas, v, w;
	std::cin >> numero_aristas;
	for (long int i = 0; i < numero_aristas; ++i) {
		std::cin >> v >> w;
		g.ponArista(v-1, w-1);
		++grado_entrada[w-1];
	}
}

int encontrar_siguiente(int N, std::set<int>& visitados, std::map<int, int>& grado_entrada) {
	for (int i = 0; i < N; ++i) {
		if (!visitados.count(i) && !grado_entrada[i])
			return i;
	}
	return -1;
}

void ordenacion(Digrafo& g, std::vector<int>& orden, std::set<int>& visitados, std::map<int, int>& grado_entrada, int origen);


int main()
{
	
	//std::ifstream in("in.txt");
	//std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!

	int N;
	bool imposible;
	std::cin >> N;
	while (std::cin) {
		std::map<int, int> grado_entrada;
		std::set <int> visitados;
		std::vector<int> orden;
		imposible = false;
		Digrafo g = Digrafo(N);
		leerGrafo(g, grado_entrada);
		while (!imposible && visitados.size() < g.V()) {
			int siguiente_nodo = encontrar_siguiente(N, visitados, grado_entrada);
			if (siguiente_nodo == -1) imposible = true;
			else ordenacion(g, orden, visitados, grado_entrada, siguiente_nodo);
		}
		if (!imposible) for (auto i : orden) std::cout << i+1 << " ";
		else std::cout << "Imposible";
		std::cout << "\n";
		std::cin >> N;
	}
}

void ordenacion(Digrafo &g, std::vector<int> &orden, std::set<int> &visitados, std::map<int, int> &grado_entrada, int origen) {
	std::queue<int> siguientes;
	siguientes.push(origen);
	while (!siguientes.empty()) {
		int u = siguientes.front();
		siguientes.pop();
		visitados.insert(u);
		orden.push_back(u);
		for (auto v : g.ady(u)) {
			if (!visitados.count(v))
				if (--grado_entrada[v] <= 0)
					siguientes.push(v);
		}
	}
}