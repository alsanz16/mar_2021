#include <cmath>
#include <climits>
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <list>
#include <unordered_map>
#include <set>
#include <queue>
#include <unordered_set>
#include <vector>
#include "ConjuntosDisjuntos.h"


int main() {
    // std::ifstream in("in.txt");
    // std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!


    long int N, M;
    std::cin >> N >> M;
    long int usuarios, u1, u2;
    while (std::cin) {
        ConjuntosDisjuntos c = ConjuntosDisjuntos(N);
        for (long int i = 0; i < M; ++i) {
            std::cin >> usuarios;
            if (usuarios) std::cin >> u1;
            for (long int j = 1; j < usuarios; ++j) {
                std::cin >> u2;
                c.unir(u1-1, u2-1);
            }
        }
        for (long int i = 0 ; i < N; ++i)
            std::cout << c.cardinal(i) << " ";
        std::cout << "\n";
        std::cin >> N >> M;
    }
}
