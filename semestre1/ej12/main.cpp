// prueba.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <set>
#include <unordered_map>
#include <vector>
#include "ConjuntosDisjuntos.h"
using T = std::pair<int,int>;


int main()
{
    //std::ifstream in("in.txt");
    //std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!

    char caracter_leido;
    int filas, columnas, contador;
    std::cin >> filas >> columnas;
    while (std::cin) {
        contador = 0;
        std::map<T, int> posiciones; 
        for (int i = 0; i < filas; ++i) {
            for (int j = 0; j < columnas; ++j) {
                std::cin >> caracter_leido;
                if (caracter_leido == '#')
                    posiciones[{i, j}] = contador++;
            }
        }
        ConjuntosDisjuntos c = ConjuntosDisjuntos(posiciones.size());
        for (auto it = posiciones.begin(); it != posiciones.end(); ++it) {
            T anterior_izquierda = { it->first.first - 1, it->first.second };
            T anterior_arriba = { it->first.first, it->first.second - 1 };
            if (posiciones.count(anterior_izquierda))
                c.unir(it->second, posiciones[anterior_izquierda]);
            if (posiciones.count(anterior_arriba))
                c.unir(it->second, posiciones[anterior_arriba]);
        }

        int maximo = 0;
        for (unsigned int i = 0; i < posiciones.size(); ++i)
            maximo = std::max(maximo, c.cardinal(i));
        
        std::cout << c.num_cjtos() << " " << maximo << "\n";
        std::cin >> filas >> columnas;

    }
}
