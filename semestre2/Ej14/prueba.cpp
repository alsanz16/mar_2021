#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using bVec = std::vector<bool>;
using bMatrix = std::vector<std::vector<bool>>;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tCube = std::vector<tMatrix>;
const T INF = INT_MAX;
const T NEG_INF = INT_MIN;


void calcular_costes(const tMatrix &tarifas, tMatrix &costes) {
    T N = tarifas.size();
    for (T k = 2; k < N; ++k) {
        for (T i = 0, j = k; j < N; ++i, ++j) {
            T minimo = INF;
            for (T s = i; s < j; ++s)
                costes[i][j] = std::min(costes[i][j], costes[i][s] + costes[s][j]);
        }
    }
}

int main()
{
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

    T N;
    std::cin >> N;
    while (std::cin) {
        tMatrix tarifas (N, tVec(N , INF));
        for (T k = 0; k < N; ++k) {
            tarifas[k][k] = 0;
            for (T j = k + 1; j < N; ++j)
                std::cin >> tarifas[k][j];
        }
        tMatrix costes = tarifas;
        calcular_costes(tarifas, costes);
        for (T k = 0; k < N; ++k) {
            for (T j = k+1; j < N; ++j)
                std::cout << costes[k][j] << " ";
            std::cout << "\n";
        }
        std::cin >> N;

    }
}