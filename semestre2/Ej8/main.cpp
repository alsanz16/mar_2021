#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using bVec = std::vector<bool>;
using bMatrix = std::vector<std::vector<bool>>;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tPair = std::pair<T,T>;
using tStack = std::stack<T>;
void reconstruir_solucion(const tMatrix &subsecuencia, const std::string &s1, const std::string &s2, std::string &solucion);

const T MAX = INT_MAX;

T grados_separacion(const bMatrix &relaciones) {
    T N = relaciones.size();
    tMatrix separacion(N, tVec(N, MAX));
        
    for (T i = 0; i < N; ++i) {
        for (T j = 0; j < N; ++j) {
            if (i == j) separacion[i][j] = 0;
            else if (relaciones[i][j]) separacion[i][j] = 1;
        }
    }
    
    T maxima_separacion = 0;
    for (T k = 1; k < N+1; ++k) {
        for (T i = 0; i < N; ++i) {
            for (T j = 0; j < N; ++j) {
                separacion[i][j] = std::min(separacion[i][j], separacion[i][k-1] + separacion[k-1][j]);
                if (k == N)
                    maxima_separacion = std::max(maxima_separacion, separacion[i][j]);
            }
        }
    }
    return maxima_separacion;
}

int main() {
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
    
    T P, R;
    
    std::cin >> P >> R;
    while (std::cin) {
        std::map<std::string, T> personas;
        bMatrix relaciones(P, bVec(P, false));
        
        T num[2];
        T k = 0;
        for (T i = 0; i < R; ++i) {
            std::string nombre;
            for (T j = 0; j < 2; ++j) {
                std::cin >> nombre;
                if (!personas.count(nombre))
                    personas[nombre] = k++;
                num[j] = personas[nombre];
            }
            relaciones[num[0]][num[1]] = relaciones[num[1]][num[0]] = true;
        }
        
        T sep = grados_separacion(relaciones);
        if (sep >= MAX) std::cout << "DESCONECTADA\n";
        else std::cout << grados_separacion(relaciones) << "\n";
        std::cin >> P >> R;
    }
}
