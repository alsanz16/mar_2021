#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tPair = std::pair<T,T>;
using tStack = std::stack<T>;

const T MAX = INT_MAX;
void reconstruir_solucion(const tMatrix &cambios, const std::string &palabra, std::string &palindromo);

T cambios(const std::string &palabra, std::string &palindromo) {
    T N = palabra.size();
    tMatrix cambios(N, tVec(N, 0));
    
    for(T k = 1; k < N; ++k)
        for (T i = 0, j = k; j < N; ++i, ++j)
            if (palabra[i] == palabra[j])
                cambios[i][j] =  cambios[i+1][j-1];
            else
                cambios[i][j] = std::min(cambios[i+1][j], cambios[i][j-1]) + 1;
    
    reconstruir_solucion(cambios, palabra, palindromo);
    return cambios[0][N-1];
}

void reconstruir_solucion(const tMatrix &cambios, const std::string &palabra, std::string &palindromo) {
    T N = palabra.size();
    T i_sol = 0;
    T j_sol = N-1;
    
    T i_p = 0;
    T j_p = N - 1 + cambios[0][N-1];
    
    palindromo.resize(N + cambios[0][N-1]);
    
    while (cambios[i_sol][j_sol] != 0) {
        if (cambios[i_sol][j_sol] == cambios[i_sol][j_sol-1] + 1) {
            palindromo[i_p] = palabra[j_sol];
            palindromo[j_p] = palabra[j_sol];
            --j_sol;
            ++i_p;
            --j_p;
        }
        else if (cambios[i_sol][j_sol] == cambios[i_sol+1][j_sol] + 1) {
            palindromo[i_p] = palabra[i_sol];
            palindromo[j_p] = palabra[i_sol];
            ++i_sol;
            ++i_p;
            --j_p;
        }
        else {
            palindromo[i_p] = palindromo[j_p] = palabra[i_sol];
            ++i_sol;
            --j_sol;
            ++i_p;
            --j_p;
        }

    }
    for (T i = i_sol; i <= j_sol; ++i)
        palindromo[i_p++] = palabra[i];
}

int main()
{
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
    
    std::string palabra;
    std::cin >> palabra;
    while (std::cin) {
        std::string palindromo;
        std::cout << cambios(palabra, palindromo) << " ";
        for (auto c: palindromo)
            std::cout << c;
        std::cout << "\n";
        std::cin >> palabra;
    }
}
