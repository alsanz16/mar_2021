#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using bVec = std::vector<bool>;
using bMatrix = std::vector<std::vector<bool>>;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tCube = std::vector<tMatrix>;
const T INF = INT_MAX;
const T NEG_INF = INT_MIN;


T num_trozos(const tVec &bizcocho) {
    T N = bizcocho.size();
    tMatrix trozos(N, tVec(N, 0));
    for (T k = 2; k < N; ++k) {
        for (T i = 0, j = k; j < N; ++i, ++j) {
            trozos[i][j] = std::max({ trozos[i + 1][j - 1] + (bizcocho[i] && (bizcocho[i] == bizcocho[j])), trozos[i + 2][j], trozos[i][j - 2] });
        }
    }
    return trozos[0][N - 1];
}

int main()
{
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

    T N;
    std::cin >> N;
    while (std::cin) {
        tVec bizcocho(N);
        for (T i = 0; i < N; ++i) std::cin >> bizcocho[i];
        std::cout << num_trozos(bizcocho) << "\n";
        std::cin >> N;

    }
}