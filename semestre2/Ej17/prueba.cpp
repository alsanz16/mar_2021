#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using bVec = std::vector<bool>;
using bMatrix = std::vector<std::vector<bool>>;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tCube = std::vector<tMatrix>;
const T INF = INT_MAX;
const T NEG_INF = INT_MIN;


T num_trozos(const bMatrix &pasos) {
    T N = pasos.size();
    T M = pasos[0].size();
    tMatrix caminos(N, tVec(M, 0));
    caminos[0][0] = !pasos[0][0];

    /*
    Recorremos las diagonales secundarias de la ciudad empezando por la 
    esquina superior izquierda (m�s facil que empezando por la esquina inferior derecha)
    */
    for (T k = 1; k < M; ++k) {
        for (T i = 0, j = k; i < N && j >= 0; ++i, --j) {
            if (pasos[i][j]) caminos[i][j] = 0;
            else caminos[i][j] = (i > 0 ? caminos[i - 1][j] : 0) + (j > 0 ? caminos[i][j - 1] : 0);
        }
    }

    for (T k = 1; k < N; ++k) {
        for (T i = k, j = M-1; i < N && j >= 0; ++i, --j) {
            if (pasos[i][j]) caminos[i][j] = 0;
            else caminos[i][j] = (i > 0 ? caminos[i - 1][j] : 0) + (j > 0 ? caminos[i][j - 1] : 0);
        }
    }


    return caminos[N-1][M-1];
}

int main()
{
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

    char c;
    T N, M;
    std::cin >> N >> M;
    while (std::cin) {
        bMatrix pasos(N, bVec(M, false));
        for (T i = 0; i < N; ++i) {
            for (T j = 0; j < M; ++j) {
                std::cin >> c;
                pasos[i][j] = (c == 'P');
            }
        }
        std::cout << num_trozos(pasos) << "\n";
        std::cin >> N >> M;

    }
}