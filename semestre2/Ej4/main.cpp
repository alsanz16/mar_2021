#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using tVec = std::vector<T>;
using tMatrix = std::vector<tAVec>;
using tPair = std::pair<T,T>;

const T MAX = INT_MAX;

tPair coste_minimo(const tVec &costes, const tVec &potencia, T potencia_min, T potencia_max) {
    T N = costes.size();
    tMatrix min_costes(N+1, tVec(potencia_max+1, MAX));
    
    min_costes[0][0] = 0;
    for (T i = 1; i < N + 1; ++i)
        for (T j = 0;  j < potencia_max+1; ++j)
            if (j - potencia[i-1] >= 0)
                min_costes[i][j] = std::min(min_costes[i-1][j], min_costes[i][j-potencia[i-1]] + costes[i-1]);
            else min_costes[i][j] = min_costes[i-1][j];
    
    T coste = MAX;
    T potencia_conseguida;
    for (T i = potencia_min; i < potencia_max + 1; ++i)
    if (min_costes[N][i] < coste) {
        coste = min_costes[N][i];
        potencia_conseguida = i;
    }
    
    return {coste, potencia_conseguida};
}
int main()
{
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

    T N, potencia_max, potencia_min;
    std::cin >> N >> potencia_max >> potencia_min;
    while (std::cin) {
        tVec costes(N);
        tVec potencia(N);
        for (T i = 0; i < N; ++i)
            std::cin >> potencia[i];
        for (T i = 0; i < N; ++i)
            std::cin >> costes[i];

        tPair res = coste_minimo(costes, potencia, potencia_min, potencia_max);
        if (res.first >= MAX) std::cout << "IMPOSIBLE\n";
        else std::cout << res.first << " " << res.second << "\n";
        std::cin >> N >> potencia_max >> potencia_min;
    }
}
