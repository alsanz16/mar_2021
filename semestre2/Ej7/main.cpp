#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tPair = std::pair<T,T>;
using tStack = std::stack<T>;
void reconstruir_solucion(const tMatrix &subsecuencia, const std::string &s1, const std::string &s2, std::string &solucion);

T mayor_subsecuencia(const std::string &s1, const std::string &s2, std::string &solucion) {
    T N1 = s1.size();
    T N2 = s2.size();
    tMatrix subsecuencia(N1+1, tVec(N2+1, 0));
    
    for (T i = 1; i < N1 +1 ; ++i)
        for (T j = 1; j < N2 + 1; ++j)
            if (s1[i-1] == s2[j-1])
                subsecuencia[i][j] = subsecuencia[i-1][j-1] + 1;
            else
                subsecuencia[i][j] = std::max(subsecuencia[i-1][j], subsecuencia[i][j-1]);
    
    reconstruir_solucion(subsecuencia, s1, s2, solucion);
    return subsecuencia[N1][N2];
}

void reconstruir_solucion(const tMatrix &subsecuencia, const std::string &s1, const std::string &s2, std::string &solucion) {
    T N1 = s1.size();
    T N2 = s2.size();
    T M = subsecuencia[N1][N2];
    solucion.resize(M);
    
    T i_sol = N1;
    T j_sol = N2;
    T i_p = M-1;
    while (subsecuencia[i_sol][j_sol] != 0) {
        if (subsecuencia[i_sol][j_sol] == subsecuencia[i_sol-1][j_sol] )
            --i_sol;
        else if (subsecuencia[i_sol][j_sol] == subsecuencia[i_sol][j_sol-1]) --j_sol;
        else {
            solucion[i_p--] = s1[i_sol - 1];
            --i_sol;
            --j_sol;
        }

    }
}

int main() {
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
    std::string s1, s2;
    std::cin >> s1 >> s2;
    while (std::cin) {
        std::string solucion;
        mayor_subsecuencia(s1, s2, solucion);
        std::cout << solucion << "\n";
        std::cin >> s1 >> s2;
    }
}
