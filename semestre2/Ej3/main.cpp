#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tPair = std::pair<T,T>;

const T MAX = INT_MAX;

tPair valor_max(const tMatrix &puntuaciones) {
    T N = puntuaciones.size();
    tMatrix maximo(N, tVec(N));
    for (T i = 0; i < N-1; ++i)
        for (T j = 0;  j < N; ++j)
            maximo [i][j] = 0;
    
    for (T j = 0; j < N; ++j) maximo[0][j] = puntuaciones[0][j];
    
    for (T i = 1; i < N; ++i) {
        for (T j = 0; j < N; ++j) {
            if (j == 0 ) maximo[i][j] = std::max(maximo[i-1][j], maximo[i-1][j+1]);
            else if (j == N-1 ) maximo[i][j] = std::max(maximo[i-1][j-1], maximo[i-1][j]);
            else maximo[i][j] = std::max({maximo[i-1][j-1], maximo[i-1][j], maximo[i-1][j+1]});
            maximo[i][j] += puntuaciones[i][j];
        }
    }
    
    T pos;
    T max = 0;
    for (T j = 0; j < N; ++j) {
        if (maximo[N-1][j] > max) {
            max = maximo[N-1][j];
            pos = j;
        }
    }
    return {max, pos+1};
}

int main()
{
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

    T N;
    std::cin >> N;
    while (std::cin) {
        tMatrix puntuaciones(N, tVec(N));
        
        for (T i = 0; i < N; ++i)
            for (T j = 0; j < N; ++j)
                std::cin >> puntuaciones[i][j];
        tPair res = valor_max(puntuaciones);
        std::cout << res.first << " " << res.second << "\n";
        std::cin >> N;
    }
}
