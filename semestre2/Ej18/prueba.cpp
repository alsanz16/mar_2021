#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using bVec = std::vector<bool>;
using bMatrix = std::vector<std::vector<bool>>;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tCube = std::vector<tMatrix>;
using tQueue = std::priority_queue<T>;
using tPair = std::pair<T, T>;
const T INF = INT_MAX;
const T NEG_INF = INT_MIN;

struct solucion {
    T i;
    T tiempo_estimado;
    T tiempo_actual;
    bVec repartidos;

    solucion(
        solucion& sol,  // soluci�n a partir de la cual se contruye la nueva
        T tarea,       // carretera asignada a la quitanieves s.index
        T tiempo,  // calidad de la selecci�n escogida
        T estimacion      // cota superior de la soluci�n
    ) : i(sol.i + 1),
        tiempo_actual(sol.tiempo_actual + tiempo),
        tiempo_estimado(estimacion),
        repartidos(sol.repartidos)
    {
        repartidos[tarea] = true;
    }


    solucion(T N) :
        i(0),
        tiempo_estimado(0),
        tiempo_actual(0),
        repartidos(N, false)
    { 
    }

};


bool compara(const solucion& s1, const solucion& s2) {
    return s1.tiempo_estimado > s2.tiempo_estimado;

}

tVec rapido;
T cota_optimista(solucion& sol) {
    return rapido[sol.i ] + sol.tiempo_actual;
}

tVec lento;
T cota_pesimista(solucion& sol) {
    return lento[sol.i ] + sol.tiempo_actual;
}

auto comp = [](const solucion& s1, const solucion& s2) { return compara(s1, s2); };
T min_horas(T N, tMatrix& horas) {
    std::priority_queue<solucion, std::vector<solucion>, decltype(comp)> pq(comp);
    solucion inicio(N);
    pq.push(inicio);

    T pesimista;
    T coste_mejor = cota_pesimista(inicio);
    while (!pq.empty() && pq.top().tiempo_estimado < coste_mejor) {
        solucion Y = pq.top();
        pq.pop();

        if (Y.i == N) 
            coste_mejor = std::min(coste_mejor, Y.tiempo_actual);
        else {
            for (T j = 0; j < N; ++j) {
                if (!Y.repartidos[j]) {
                    solucion X(Y, j, horas[Y.i][j], Y.tiempo_estimado);
                    if ((X.tiempo_estimado = cota_optimista(X)) < coste_mejor) {
                        pq.push(X);
                        if ((pesimista = cota_pesimista(X)) < coste_mejor)
                            coste_mejor = pesimista;
                    }
                }
            }
        }
    }
    return coste_mejor;
}


int main() {
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
    T N;
    std::cin >> N;
    while (N) {
        tMatrix horas = tMatrix(N, tVec(N));
        // [i][j]: horas del funcionario i para hacer
        //  la tarea j
        for (T i = 0; i < N; ++i)
            for (T j = 0; j < N; ++j)
                std::cin >> horas[i][j];

        rapido = tVec(N+1);
        lento = tVec(N + 1);
        rapido[N] = lento[N] = 0;

        for (T i = N - 1; i >= 0; --i) {
            T minimo = INF;
            for (T j = 0; j < N; ++j)
                minimo = std::min(minimo, horas[i][j]);
            rapido[i] = minimo + (i == N - 1 ? 0 : rapido[i + 1]);
        }

        for (T i = N - 1; i >= 0; --i) {
            T maximo = 0;
            for (T j = 0; j < N; ++j)
                maximo = std::max(maximo, horas[i][j]);
            lento[i] = maximo + (i == N - 1 ? 0 : lento[i + 1]);
        }

        std::cout << min_horas(N, horas) << std::endl;
        std::cin >> N;
    }
}
