#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
struct tRet {
    T formas;
    T cuerdas;
    T coste;
};

const T MAX = pow(10, 18);

tRet formas(const tVec &longitudes, const tVec &coste, T L) {
    tVec formas(L+1, 0);
    formas[0] = 1;
    
    tVec cuerdas(L + 1, MAX);
    cuerdas[0] = 0;

    tVec precio(L + 1, MAX);
    precio[0] = 0;


    T N = longitudes.size();
    for (T i = 0; i < N; ++i) {
        for (T l = L; l >= 0; --l) {
            if (l >= longitudes[i]) {
                formas[l] = formas[l] + formas[l - longitudes[i]];

                cuerdas[l] = std::min(cuerdas[l - longitudes[i]] + 1, cuerdas[l] );

                precio[l] = std::min(precio[l - longitudes[i]] + coste[i], precio[l]);
            }
        }
    }
    return {formas[L], cuerdas[L], precio[L] };
}


int main()
{
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

    T N;
    T L;
    std::cin >> N >> L;
    while (std::cin) {
        tVec longitudes(N);
        tVec coste(N);
        for (T i = 0; i < N; ++i)
            std::cin >> longitudes[i] >> coste[i];

        tRet resultado = formas(longitudes, coste, L);
        if (resultado.formas)
            std::cout << "SI " << resultado.formas << " " << resultado.cuerdas << " " << resultado.coste << "\n";
        else std::cout << "NO\n";
        std::cin >> N >> L;
    }
}