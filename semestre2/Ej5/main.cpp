#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <stack>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tPair = std::pair<T,T>;
using tStack = std::stack<T>;

const T MAX = INT_MAX;
void reconstruir_solucion(const tMatrix &cofres, const tVec &profundidad, const tVec &oro, T aire, tStack &solucion);

T max_oro(const tVec &profundidad, const tVec &oro, T aire, tStack &solucion) {
    T N = profundidad.size();
    tMatrix cofres(N+1, tVec(aire+1, 0));
    
    for (T i = 1; i < N + 1; ++i)
        for (T j = 0;  j < aire+1; ++j)
            if (j - 3*profundidad[i-1] >= 0)
                cofres[i][j] = std::max(cofres[i-1][j], cofres[i-1][j-3*profundidad[i-1]] + oro[i-1]);
            else cofres[i][j] = cofres[i-1][j];
    
    reconstruir_solucion(cofres, profundidad, oro, aire, solucion);
    return cofres[N][aire];
}

void reconstruir_solucion(const tMatrix &cofres, const tVec &profundidad, const tVec &oro, T aire, tStack &solucion) {
    T N = profundidad.size();
    T i_sol = N;
    T j_sol = aire;
    
    while(cofres[i_sol][j_sol] != 0) {
        if (cofres[i_sol][j_sol] != cofres[i_sol-1][j_sol]) {
            solucion.push(i_sol-1);
            j_sol -= 3*profundidad[i_sol-1];
        }
    --i_sol;
    }
}

int main()
{
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

    T aire, N;
    std::cin >> aire >> N;
    while (std::cin) {
        tVec profundidad(N);
        tVec oro(N);
        tStack solucion;
        for (T i = 0; i < N; ++i)
            std::cin >> profundidad[i] >> oro[i];

        T oro_max = max_oro(profundidad, oro, aire, solucion);
        std::cout << oro_max << "\n" << solucion.size() << "\n";
        while(!solucion.empty()) {
            T s = solucion.top();
            std::cout << profundidad[s] << " " << oro[s] << "\n";
            solucion.pop();
        }
        std::cout << "---\n";
        std::cin >> aire >> N;
    }
}
