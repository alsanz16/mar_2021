#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using bVec = std::vector<bool>;
using bMatrix = std::vector<std::vector<bool>>;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tCube = std::vector<tMatrix>;
using tQueue = std::priority_queue<T>;
using tPair = std::pair<T, T>;
const T INF = INT_MAX;
const T NEG_INF = INT_MIN;

struct solucion {
    T i;
    T tiempo_estimado;
    T tiempo_actual;
    bVec repartidos;

    solucion(
        solucion& sol,  // soluci�n a partir de la cual se contruye la nueva
        T tarea,       // carretera asignada a la quitanieves s.index
        T tiempo,  // calidad de la selecci�n escogida
        T estimacion      // cota superior de la soluci�n
    ) : i(sol.i + 1),
        tiempo_actual(sol.tiempo_actual + tiempo),
        tiempo_estimado(estimacion),
        repartidos(sol.repartidos)
    {
        repartidos[tarea] = true;
    }


    solucion(T N) :
        i(0),
        tiempo_estimado(0),
        tiempo_actual(0),
        repartidos(N, false)
    {
    }

};

tMatrix t;
tVec min_vec, max_vec;
bool compara(const solucion& s1, const solucion& s2) {
    return s1.tiempo_estimado < s2.tiempo_estimado;

}

int cota_optimista(const solucion& s)
{
    return s.tiempo_actual + max_vec[s.i-1];
}

int cota_pesimista(const solucion& s)
{
    return s.tiempo_actual;
}

auto comp = [](const solucion& s1, const solucion& s2) { return compara(s1, s2); };
T min_horas(tMatrix& horas, const tVec& longitud, const tVec& anchura) {
    T N = horas.size();
    if (N == 0) return 0;
    T M = horas[0].size() - 1;

    std::priority_queue<solucion, std::vector<solucion>, decltype(comp)> pq(comp);
    solucion inicio(M + 1);
    inicio.tiempo_estimado = INF;
    pq.push(inicio);

    T pesimista;
    T coste_mejor = cota_pesimista(inicio);
    while (!pq.empty() && pq.top().tiempo_estimado > coste_mejor) {
        solucion Y = pq.top();
        pq.pop();

        if (Y.i == N)
            coste_mejor = std::max(coste_mejor, Y.tiempo_actual);
        else {
            for (T j = 0; j <= M; ++j) {
                if (!Y.repartidos[j] && longitud[Y.i] <= anchura[j]) {
                    solucion X(Y, j, horas[Y.i][j], Y.tiempo_estimado);
                    X.repartidos[M] = false;
                    if ((X.tiempo_estimado = cota_optimista(X)) > coste_mejor) {
                        pq.push(X);
                        if ((pesimista = cota_pesimista(X)) > coste_mejor)
                            coste_mejor = pesimista;
                    }
                }
            }
        }
    }
    return coste_mejor;
}


int main() {
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
    T casos, N, M;
    std::cin >> casos;
    for (T i = 0; i < casos; ++i) {
        std::cin >> N >> M;
        tVec longitud(N), anchuras(M + 1);
        for (T i = 0; i < N; ++i) std::cin >> longitud[i];
        for (T i = 0; i < M; ++i) std::cin >> anchuras[i];
        anchuras[M] = INF;

        tMatrix coste = tMatrix(N, tVec(M + 1));
        for (T i = 0; i < N; ++i)
            for (T j = 0; j < M; ++j)
                std::cin >> coste[i][j];

        for (T i = 0; i < N; ++i) coste[i][M] = 0;

        max_vec = tVec(N + 1);
        max_vec[N] = min_vec[N] = 0;

        for (T i = N - 1; i >= 0; --i) {
            T maximo = 0;
            for (T j = 0; j < N; ++j)
                maximo = std::max(maximo, coste[i][j]);
            max_vec[i] = maximo + max_vec[i + 1];
        }


        std::cout << min_horas(coste, longitud, anchuras) << std::endl;
    }
}
