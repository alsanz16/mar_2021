#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using bVec = std::vector<bool>;
using bMatrix = std::vector<std::vector<bool>>;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tCube  = std::vector<tMatrix>;
const T INF = INT_MAX;
const T NEG_INF = INT_MIN;

T monedas(T pago, const tVec &valor, const tVec &cantidad, T max_cantidad) {
    T N = valor.size();
    tMatrix monedas(pago+1, tVec(max_cantidad + 1, INF));
    for (T j = 0; j < max_cantidad+1; ++j) monedas[0][j]  =0;

    // k tipos de monedas usadas
    // j numero de monedas usadas del tipo k
    // i valor a pagar
    for (T k = 0; k < N; ++k)
        for (T i = 1; i < pago + 1; ++i)
            for (T j = 0; j < cantidad[k] + 1; ++j)
                // si no se usa la moneda k, haran falta tantas monedas
                //  como hacian falta usando todas las posibles del tipo k-1
                if (j == 0) {
                    if (k != 0)
                        monedas[i][j] = monedas[i][cantidad[k-1]];
                }
                // minimo entre usar la moneda k una vez mas o no
                else if (i >= valor[k] && j < cantidad[k] + 1)
                    monedas[i][j] = std::min(monedas[i-valor[k]][j-1]+1,
                                             monedas[i][j-1]);
                
    return monedas[pago][cantidad[N-1]];
}

int main() {
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
    T N;
    std::cin >> N;
    while (std::cin) {
        T pago;
        T max_cantidad = 0;
        tVec valor(N), cantidad(N);
        for (int i = 0; i < N; ++i) std::cin >> valor[i];
        for (int i = 0; i < N; ++i) {
            std::cin >> cantidad[i];
            max_cantidad = std::max(max_cantidad, cantidad[i]);
        }
        std::cin >> pago;
        T monedas_necesarias = monedas(pago,valor, cantidad, max_cantidad);
        if (monedas_necesarias >= INF)
            std::cout << "NO\n";
        else std::cout << "SI " << monedas_necesarias << "\n";
        std::cin >> N;
    }
}
