#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <stack>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using bVec = std::vector<bool>;
using bMatrix = std::vector<std::vector<bool>>;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tPair = std::pair<T,T>;
using tStack = std::stack<T>;

const T MAX = INT_MAX;

T coste_cortar(const tVec &cortes) {
    T N = cortes.size();
    tMatrix costes(N, tVec(N, 0));
    for (T k = 2; k < N; ++k) {
        for (T i = 0, j = k; j < N; ++i, ++j) {
            T min = MAX;
            for (T s = i+1; s < j; ++s)
                min = std::min(min, costes[i][s] + costes[s][j] + 2*(cortes[j]-cortes[i]));
            costes[i][j] = min;
        }
    }
    return costes[0][N-1];
}

int main() {
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
    
    T N, L;
    std::cin >> L >> N;
    while (L && N) {
        tVec cortes(N+2);
        cortes[0] = 0;
        for (T i = 1; i < N+1; ++i)
            std::cin >> cortes[i];
        cortes[N+1] = L;
        
        std::cout << coste_cortar(cortes) << "\n";
        std::cin >> L >> N;
    }
    
}
