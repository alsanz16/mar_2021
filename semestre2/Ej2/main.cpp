#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tPair = std::pair<T,T>;

const T MAX = INT_MAX;
void reconstruir_solucion(const tVec &puntuaciones, tMatrix &puntos, T P, tVec &resultado) ;

bool formas(const tVec &puntuaciones, T P, tVec &resultados) {
    T S = puntuaciones.size();
    tMatrix puntos(S+1, tVec(P+1, MAX));
    puntos[S][0] = 0;
    for (T i = S-1; i >= 0; --i) {
        for (T j = 0; j <= P; ++j) {
            if (j >= puntuaciones[i])
                puntos[i][j] = std::min(puntos[i + 1][j], puntos[i][j - puntuaciones[i]] + 1);
            else
                puntos[i][j] = puntos[i + 1][j];
        }
    }
    // no se ha encontrado la solucion
    if (puntos[0][P] >= MAX) return false;
    
    reconstruir_solucion(puntuaciones, puntos, P, resultados);
    return true;
}

void reconstruir_solucion(const tVec &puntuaciones, tMatrix &puntos, T P, tVec &resultado) {
    T i_sol = 0;
    T j_sol = P;
    while (puntos[i_sol][j_sol] != 0) {
        if (j_sol >= puntuaciones[i_sol] &&
            puntos[i_sol][j_sol] == puntos[i_sol][j_sol - puntuaciones[i_sol]] + 1) {
            j_sol -= puntuaciones[i_sol];
            resultado.push_back(puntuaciones[i_sol]);
        }
        else if (puntos[i_sol][j_sol] == puntos[i_sol+1][j_sol]) ++i_sol;
    }
}


int main()
{
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

    T P;
    T S;
    std::cin >> P >> S;
    while (std::cin) {
        tVec puntuaciones(S);
        tVec resultado;
        for (T i = 0; i < S; ++i)
            std::cin >> puntuaciones[S-1-i];
        bool posible = formas(puntuaciones, P, resultado);
        if (posible) {
            std::cout << resultado.size() << ":";
            for (T r : resultado)
                std::cout << " " << r;
            std::cout << "\n";
        }
        else std::cout << "Imposible\n";
        std::cin >> P >> S;
    }
}
