#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using bVec = std::vector<bool>;
using bMatrix = std::vector<std::vector<bool>>;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tCube = std::vector<tMatrix>;
using tQueue = std::priority_queue<T>;
using tPair = std::pair<T, T>;
const T INF = INT_MAX;
const T NEG_INF = INT_MIN;

struct solucion {
    T i;
    T tiempo_estimado;
    T tiempo_actual;
    tVec repartidos;

    solucion(
        solucion& sol,  
        T tarea,       
        T tiempo, 
        T estimacion     
    ) : i(sol.i + 1),
        tiempo_actual(sol.tiempo_actual + tiempo),
        tiempo_estimado(estimacion),
        repartidos(sol.repartidos)
    {
        ++repartidos[tarea];
    }


    solucion(T N) :
        i(0),
        tiempo_estimado(0),
        tiempo_actual(0),
        repartidos(N, 0)
    {
    }

};

tMatrix t;
tVec min_vec, max_vec;

int cota_optimista(const solucion& s)
{
    return s.tiempo_actual + min_vec[s.i];
}

int cota_pesimista(const solucion& s)
{
    return s.tiempo_actual + max_vec[s.i];
}

auto comp = [](const solucion& s1, const solucion& s2) { return s1.tiempo_estimado > s2.tiempo_estimado; };
T min_horas(tMatrix& horas) {
    T M = horas.size();
    if (M == 0) return 0;
    T N = horas[0].size();

    std::priority_queue<solucion, std::vector<solucion>, decltype(comp)> pq(comp);
    solucion inicio(M);
    pq.push(inicio);

    T pesimista;
    T coste_mejor = cota_pesimista(inicio);
    while (!pq.empty() && pq.top().tiempo_estimado < coste_mejor) {
        solucion Y = pq.top();
        pq.pop();
        if (Y.i == N) coste_mejor = std::min(coste_mejor, Y.tiempo_actual);
        else {
            for (T j = 0; j < M; ++j) {
                if (Y.repartidos[j] < 3) {
                    solucion X(Y, j, horas[j][Y.i], Y.tiempo_estimado);
                    
                    if ((X.tiempo_estimado = cota_optimista(X)) < coste_mejor) {
                        pq.push(X);
                        if ((pesimista = cota_pesimista(X)) < coste_mejor)
                            coste_mejor = pesimista;
                    }
                }
            }
        }
    }
    return coste_mejor;
}


int main() {
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
    T casos, N, M;
    std::cin >> casos;
    for (T i = 0; i < casos; ++i) {
        std::cin >> M >> N;
        tMatrix coste = tMatrix(M, tVec(N));
        for (T i = 0; i < M; ++i)
            for (T j = 0; j < N; ++j)
                std::cin >> coste[i][j];

        max_vec = tVec(N+1,0);
        max_vec[N] = 0;
        for (T j = N - 1; j >= 0; --j) {
            for (T i = 0; i < M; ++i)
                max_vec[j] = std::max(max_vec[j], coste[i][j]);
            max_vec[j] += max_vec[j + 1];
        }

        min_vec = tVec(N + 1, INF);
        min_vec[N] = 0;
        for (T j = N - 1; j >= 0; --j) {
            for (T i = 0; i < M; ++i)
                min_vec[j] = std::min(min_vec[j], coste[i][j]);
            min_vec[j] += min_vec[j + 1];
        }

        std::cout << min_horas(coste) << std::endl;
    }
}
