#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using bVec = std::vector<bool>;
using bMatrix = std::vector<std::vector<bool>>;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tCube  = std::vector<tMatrix>;
const T INF = INT_MAX;
const T NEG_INF = INT_MIN;

T max_comida( const std::vector<T> &alimentos ) {
    T N = alimentos.size();
    tMatrix comido ( N+1, tVec(N+1, 0));
    
    for (T i = 0; i+1 < N+1; ++i)
        comido[i][i+1] = alimentos[i];
    
    // trabajamos por pares (i,j) con |j-i| >= 2
    // comido[i][j] es la maxima cantidad de alimento al
    //  comer con devoradora los alimentos
    //  [i, ..., j) = [i ... j-1]
    for (T k = 2; k < N + 1; ++k) {
        for (T i = 0, j = k; j < N + 1; ++i, ++j) {
            // comemos el alimento i.
            // devoradora se queda con el mayor entre i+1 y j-1
            T i_siguiente = i+1, j_siguiente = j;
            if (alimentos[i+1] >= alimentos[j-1]) i_siguiente = i+2;
            else j_siguiente = j-1;
            
            // comemos el alimento j-1
            // devoradora se queda con el mayor entre i y j-2
            T i_siguiente2 = i, j_siguiente2 = j-1;
            if (alimentos[i] >= alimentos[j-2]) i_siguiente2 = i+1;
            else j_siguiente2 = j-2;
            
            comido[i][j] = std::max(comido[i_siguiente][j_siguiente] + alimentos[i], comido[i_siguiente2][j_siguiente2] + alimentos[j-1]);
        }
    }
    return comido[0][N];
}

int main() {
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

    T N;
    std::cin >> N;
    while (N) {
        tVec comida(N);
        for (T i = 0; i < N; ++i) std::cin >> comida[i];
        std::cout << max_comida(comida) << "\n";
        std::cin >>N;
    }
}
