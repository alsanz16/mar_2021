#include <algorithm>
#include <climits>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using T = long long int;
using bVec = std::vector<bool>;
using bMatrix = std::vector<std::vector<bool>>;
using tVec = std::vector<T>;
using tMatrix = std::vector<tVec>;
using tCube = std::vector<tMatrix>;
const T INF = INT_MAX;
const T NEG_INF = INT_MIN;


T num_grupos(T P, tVec precios, tVec grupos) {
    T N = precios.size();
    tMatrix vistos(P + 1, tVec(N + 1, 0));

    for (T i = 0; i < P + 1; ++i) {
        for (T j = N-1; j >= 0; --j) {
            if (i >= precios[j]) 
                vistos[i][j] = std::max(vistos[i - precios[j]][j + 1] + grupos[j], vistos[i][j + 1]);
            else vistos[i][j] = vistos[i][j + 1];
        }
    }
    return vistos[P][0];
}

int main()
{
#ifndef DOMJUDGE
    std::ifstream in("in.txt");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

    T P, N;
    std::cin >> P >> N;
    while (std::cin) {
        tVec precios(N), grupos(N);
        for (T i = 0; i < N; ++i) std::cin >> grupos[i] >> precios[i];
        std::cout << num_grupos(P, precios, grupos) << "\n";
        std::cin >> P >> N;

    }
}